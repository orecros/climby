﻿using UnityEngine;
using UnityEngine.UI;

public class EnergyBarDisplay : StateMachineBehaviour {

    Image fillImage;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        fillImage = animator.transform.GetChild(0).GetComponent<Image>();

        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        fillImage.fillAmount = animator.GetFloat("chargeAmount");

        base.OnStateUpdate(animator, stateInfo, layerIndex);
    }
}
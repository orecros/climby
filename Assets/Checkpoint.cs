﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Checkpoint : MonoBehaviour
{
    public UnityEvent OnReached;

    private void Awake() {
        OnReached = new UnityEvent();
    }

    private void OnDrawGizmos() {
        Mesh checkpointMesh = Resources.Load<Mesh>("Models/checkpointIndicator");

        Gizmos.color = Color.blue;
        Gizmos.DrawMesh(checkpointMesh, transform.position, transform.rotation, Vector3.one);
    }

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")) {
            OnReached.Invoke();
        }
    }
}

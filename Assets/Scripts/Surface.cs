﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Surface
{
    Vector3 position;
    Vector3 normal;
    Collider collider;
    Brush brush;
    
    public Surface(Vector3 _position, Vector3 _normal, Collider _collider = null, Brush _brush = null)
    {
        position = _position;
        normal = _normal;
        collider = _collider;
        brush = _brush;

        if(brush == null && collider != null) {
            brush = collider.GetComponent<Brush>();
        }
    }

    public Vector3 Position {
        get { return position; }
    }
    public Vector3 Normal {
        get { return normal; }
    }
    public Collider Collider {
        get { return collider; }
    }
    public Brush Brush {
        get { return brush; }
    }
    public void UpdateCollisionPoint(Vector3 newPosition, Vector3 newNormal) {
        position = newPosition;
        normal = newNormal;
    }
}

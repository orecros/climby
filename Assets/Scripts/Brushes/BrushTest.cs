﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushTest : Brush {
    public override void EnterContact(PlayerController p, ContactType contact) {
        print("entered contact " + contact.ToString());
    }

    public override void ExitContact(PlayerController p, ContactType contact) {
        print("exitted contact " + contact.ToString());
    }

    public override void SeverContact(PlayerController p, ContactType contact) {
        print("severed contact contact " + contact.ToString());
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushConveyer : Brush
{
    public enum Direction {
        X,
        NegativeX,
        Y,
        NegativeY,
        Z,
        NegativeZ
    }
    public Direction direction = Direction.X;
    [HideInInspector] public Vector3 directionVector;
    public float Speed = 1;
    PlayerController player;
    ContactType contactType;

    // unity messages
    private void Awake() {
        Material mat = GetComponent<MeshRenderer>().material;

        mat.SetFloat("_speed", Speed);
    }
    private void OnValidate() {
        directionVector = GetDirectionVector(direction);
    }

    // internal functions
    /// <summary>
    /// converts the supplied direction enum into a vector (applying local rotation)
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    private Vector3 GetDirectionVector(Direction direction) {
        Vector3 initialVec;
        switch(direction) {
            case Direction.X:
                initialVec = new Vector3(1,0,0);
                break;
            case Direction.NegativeX:
                initialVec = new Vector3(-1,0,0);
                break;
            case Direction.Y:
                initialVec = new Vector3(0,1,0);
                break;
            case Direction.NegativeY:
                initialVec = new Vector3(0,-1,0);
                break;
            case Direction.Z:
                initialVec = new Vector3(0,0,1);
                break;
            default: //Direction.NegativeZ:
                initialVec = new Vector3(0,0,-1);
                break;
        }

        return transform.rotation * initialVec;
    }

    // brush overrides
    public override void EnterContact(PlayerController p, ContactType contact) {
        player = p;

        // set the player's reference frame to the conveyer's
        player.referenceVelocity = Speed * directionVector;
    }
    public override void ExitContact(PlayerController p, ContactType contact) {

        //reset the player's reference frame to be the world's
        if(player != null) player.referenceVelocity = Vector3.zero;

        // finally, remove the player
        player = null;
    }
    public override void SeverContact(PlayerController p, ContactType contact) {

        //reset the player's reference frame to be the world's
        if (player != null) player.referenceVelocity = Vector3.zero;

        // finally, remove the player
        player = null;
    }
}

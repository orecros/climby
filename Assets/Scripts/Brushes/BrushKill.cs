﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DeathType { Goop, Shock }

public class BrushKill : Brush
{
    
    public DeathType deathType;

    public override void EnterContact(PlayerController p, ContactType contact) {
        StartCoroutine(KillAfterFrame(p));
    }

    IEnumerator KillAfterFrame(PlayerController p) {
        yield return new WaitForEndOfFrame();
        p.Kill(deathType);
    } 
}

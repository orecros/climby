﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Brush : MonoBehaviour
{
    public enum ContactType { Floor, Wall }
        
    /// <summary>
    /// called when the player dies or teleports while touching the surface
    /// </summary>
    /// <param name="contact"></param>
    public virtual void SeverContact(PlayerController p, ContactType contact) { }
    /// <summary>
    /// called when the player touches this surface. can return TRUE to stop movement application early
    /// </summary>
    /// <param name="p"></param>
    /// <param name="contact"></param>
    public virtual void EnterContact(PlayerController p, ContactType contact) { }
    /// <summary>
    /// called when the player stops touching this surface
    /// </summary>
    /// <param name="p"></param>
    /// <param name="contact"></param>
    public virtual void ExitContact(PlayerController p, ContactType contact) { }
}

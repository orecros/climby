﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushCannon : Brush
{
    public float LaunchSpeed;

    public override void ExitContact(PlayerController p, ContactType contact) {
        p.vel.y = LaunchSpeed;

        base.ExitContact(p, contact);
    }

    public float CalculateLaunchSpeed(float targetHeightChange) {
        float gravity = Mathf.Abs(Physics.gravity.y);

        return Mathf.Sqrt(2 * gravity * targetHeightChange);
    }
}

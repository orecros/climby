﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushIce : Brush {
    public override void EnterContact(PlayerController p, ContactType contact) {
        if(contact == ContactType.Floor) {
            //p.groundMoveType = PlayerController.GroundMoveType.Ice;
        }
    }

    public override void ExitContact(PlayerController p, ContactType contact) {
        if (contact == ContactType.Floor) {
            //p.groundMoveType = PlayerController.GroundMoveType.Standard;
        }
    }

    public override void SeverContact(PlayerController p, ContactType contact) {
        if (contact == ContactType.Floor) {
            //p.groundMoveType = PlayerController.GroundMoveType.Standard;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour 
{
    public Transition ExitTransition;

    public Transform EntranceTransform;
    public Transform ExitTransform;

    private void OnDrawGizmos() {
        Mesh enterIndicatorMesh = Resources.Load<Mesh>("Models/enterIndicator");
        Mesh exitIndicatorMesh = Resources.Load<Mesh>("Models/exitIndicator");

        

        if ( EntranceTransform != null ) {
            Gizmos.color = Color.green;
            Gizmos.DrawMesh(enterIndicatorMesh, EntranceTransform.position, EntranceTransform.rotation, Vector3.one);
        }
        
        if (ExitTransform != null) {
            Gizmos.color = Color.red;
            Gizmos.DrawMesh(exitIndicatorMesh, ExitTransform.position, ExitTransform.rotation, Vector3.one);
        }
    }

    public void AlignAsNext(Transform lastTransform) {
        transform.rotation = lastTransform.rotation * Quaternion.Inverse(EntranceTransform.rotation);

        transform.position = lastTransform.position + (transform.position - EntranceTransform.position);
    }

    public Room MakeNextRoom() {
        GameObject nextRoomObject = UnityEditor.PrefabUtility.InstantiatePrefab(ExitTransition.GetNextRoom()) as GameObject;
        Room r = nextRoomObject.GetComponent<Room>();
        r.AlignAsNext(ExitTransform);
        return r;
    }

    public Room MakeFinalRoom() {
        GameObject nextRoomObject = UnityEditor.PrefabUtility.InstantiatePrefab(ExitTransition.GetFinalRoom()) as GameObject;
        Room r = nextRoomObject.GetComponent<Room>();
        r.AlignAsNext(ExitTransform);
        return r;
    }
}

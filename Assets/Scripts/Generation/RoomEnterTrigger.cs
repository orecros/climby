﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ZoneTrigger : MonoBehaviour
{
    public UnityEvent OnPlayerEnter;

    // Start is called before the first frame update
    private void Awake() {
        BoxCollider b = gameObject.AddComponent<BoxCollider>();
        b.isTrigger = true;
    }

    private void OnCollisionEnter(Collision collision) {
        if(collision.transform.CompareTag("Player")) {
            OnPlayerEnter.Invoke();
        }
    }
}

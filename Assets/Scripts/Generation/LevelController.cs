﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public static LevelController Instance;
    public PlayerController player;
    public Zone FillerZone;
    public Zone[] Zones;
    [Tooltip(tooltip: "How many zones ahead of the currently active zone to maintain")]
    public int AheadZones;
    [Tooltip(tooltip:"How many zones behind the currently active zone to maintain")]
    public int BehindZones;

    public List<ZoneInstance> CurrentZoneInstances;
    ZoneInstance nextZone;
    ZoneInstance currentZone;
    public ZoneInstance CurrentZone {
        get { return currentZone; }
        set {
            if(nextZone != null) {
                // if we had a previously active next zone, we need to remove our listener from it
                nextZone.checkpoint.OnReached.RemoveListener(IncrementCurrentZone);
            }

            // find and set up our new nextzone
            int index = CurrentZoneInstances.IndexOf(value);
            nextZone = CurrentZoneInstances[index + 1];
            nextZone.checkpoint.OnReached.AddListener(IncrementCurrentZone);

            // update the value of our current zone
            currentZone = value;
        }
    }
    public ZoneInstance NextZone {
        get {
            return nextZone;
        }
    }

    protected virtual void Awake() {
        Instance = this;
        InitializeZones();
    }
    private void InitializeZones() {

        // calculate the total amount of zones that should be generated at once
        int zoneCount = 1 + AheadZones + BehindZones;

        // get the current amount of max zones
        int initialZoneCount = CurrentZoneInstances.Count;

        // create enough zons to fill the list
        for( int n = initialZoneCount; n < zoneCount; n++ ) {
            AddNewZone();
        }

        // set the current zone to be the very first zone
        CurrentZone = GetOldestZoneInstance();
    }
    private void IncrementCurrentZone() {
        // start by getting the index of the previously active zone
        CurrentZone = NextZone;

        int newIndex = CurrentZoneInstances.IndexOf(CurrentZone);
        // if we have too many zones behind
        if(newIndex > BehindZones) {
            // destroy the oldest zone
            DestroyOldestZone();
            // and add a new zone at the end
            AddNewZone();
        } 
    }
    public virtual Transform GetCurrentCheckpoint() {
        return CurrentZone.checkpoint.transform;
    }
    
    public ZoneInstance AddNewZone() {
        Zone lastZoneType = GetNewestZoneInstance().zoneType;

        if(lastZoneType == FillerZone) {
            return AddNewZone(PickRandomZone());
        }
        else {
            return AddNewZone(FillerZone);
        }
    }
    public ZoneInstance AddNewZone(Zone zoneType) {
        ZoneInstance newZone = zoneType.BuildZone(GetNewestZoneInstance());
        CurrentZoneInstances.Add(newZone);
        return newZone;
    }
    public void DestroyOldestZone() {

        // destroy the oldest zone and remove it from our list
        ZoneInstance destroyZone = GetOldestZoneInstance();
        CurrentZoneInstances.RemoveAt(0);
        Destroy(destroyZone.gameObject);

        // tell the new oldest zone to seal the entrance
        GetOldestZoneInstance().CloseEntryDoor();
    }
    Zone PickRandomZone() {
        return Zones[Mathf.FloorToInt(Random.value * Zones.Length)];
    }
    ZoneInstance GetNewestZoneInstance() {
        return CurrentZoneInstances[CurrentZoneInstances.Count - 1];
    }
    ZoneInstance GetOldestZoneInstance() {
        return CurrentZoneInstances[0];
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Single Transition", menuName = "Objects/Single-Transition")]
public class TransitionSingle : Transition
{
    public GameObject[] NextRooms;
    public GameObject FinalRoom;

    public override GameObject GetFinalRoom() {
        return FinalRoom;
    }

    public override GameObject GetNextRoom() {
        return NextRooms[Mathf.FloorToInt(Random.value * NextRooms.Length)];
    }
}

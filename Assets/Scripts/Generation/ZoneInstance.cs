﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ZoneInstance : MonoBehaviour {
    public Room firstRoom;
    [HideInInspector] public Room lastRoom;
    [HideInInspector] public Zone zoneType;
    public Checkpoint checkpoint;
    public GameObject entryDoor;
    public Transform OverrideExitTransform = null;

    private void Awake() {
        //BoxCollider b = gameObject.AddComponent<BoxCollider>();
        //b.isTrigger = true;
        firstRoom = GetComponent<Room>();
    }

    public void CloseEntryDoor() {
        entryDoor.SetActive(true);
    }

    public Transform GetExitTransform() {
        if (OverrideExitTransform != null) return OverrideExitTransform;
        return lastRoom.ExitTransform;
    }
}
    

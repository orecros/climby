﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Multi Transition", menuName = "Objects/Multi-Transition")]
public class TransitionMulti : Transition {
    public Transition[] PossibleTransitions;

    public override GameObject GetFinalRoom() {
        int transitionIndex = Mathf.FloorToInt(Random.value * PossibleTransitions.Length);

        return PossibleTransitions[transitionIndex].GetFinalRoom();
    }

    public override GameObject GetNextRoom() {
        int transitionIndex = Mathf.FloorToInt(Random.value * PossibleTransitions.Length);

        return PossibleTransitions[transitionIndex].GetNextRoom();
    }
}

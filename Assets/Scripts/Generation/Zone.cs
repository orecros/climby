﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Zone", menuName = "Objects/Zone")]
public class Zone : ScriptableObject {
    public GameObject InitialRoom;
    public int minInteriorRoomCount;
    public int maxInteriorRoomCount;

    public ZoneInstance BuildZone(ZoneInstance lastZone, Room startingRoom = null) {

        // first, generate the initial room if it doesnt already exist
        if(startingRoom == null) {
            startingRoom = Instantiate(InitialRoom).GetComponent<Room>();

            // align the initial room with the previous zone
            startingRoom.AlignAsNext(lastZone.GetExitTransform());
        }

        // Get the attached zone object
        ZoneInstance newZone = startingRoom.gameObject.GetComponent<ZoneInstance>();
        newZone.zoneType = this;

        Room r = startingRoom;

        // generate the interior rooms
        for (int n = 0; n < GenerateInteriorRoomCount(); n++) {
            //r is the latest room from the previous loop

            // make a new compatible room and align it with the previous one
            r = r.MakeNextRoom();

            // make the new room a child of our ZoneInstance
            r.transform.SetParent(startingRoom.transform);
        }

        // generate the final room
        Room lastRoom = r.MakeFinalRoom();

        // make this room a child of our ZoneInstance
        lastRoom.transform.SetParent(startingRoom.transform);

        // mark our final room as the last room of our new zone
        newZone.lastRoom = lastRoom;

        return newZone;
    }

    int GenerateInteriorRoomCount() {
        return Mathf.FloorToInt(Random.value * (maxInteriorRoomCount - minInteriorRoomCount)) + minInteriorRoomCount;
    }
}
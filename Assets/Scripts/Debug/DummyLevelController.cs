﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyLevelController : LevelController
{
    public Transform checkpoint;

    protected override void Awake() {
        Instance = this;
    }
    public override Transform GetCurrentCheckpoint() {
        return checkpoint;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayGizmo : MonoBehaviour
{
    public Mesh displayMesh;
    public Color color;

    private void OnDrawGizmos() {
        Gizmos.color = color;
        Gizmos.DrawMesh(displayMesh, transform.position, transform.rotation, Vector3.one);
    }
}

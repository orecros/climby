﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public abstract class Trigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            DoTrigger(other.GetComponent<PlayerController>());
        }
    }

    protected abstract void DoTrigger(PlayerController p);
}

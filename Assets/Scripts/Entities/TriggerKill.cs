﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerKill : Trigger
{
    public DeathType deathType;

    protected override void DoTrigger(PlayerController p) {
        StartCoroutine(KillAfterFrame(p));
    }

    IEnumerator KillAfterFrame(PlayerController p) {
        yield return new WaitForEndOfFrame();
        p.Kill(deathType);
    }
}

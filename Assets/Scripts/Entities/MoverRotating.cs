﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverRotating : Mover
{
    public float angularSpeed;
    public bool randomRotationDirection = false;
    public bool randomInitialRotation = false;
    [Range(min:1,max:180)]public int randomRotationIncrements = 45;

    private void Start() {
        if(randomRotationDirection) {
            if(Random.value * 2 > 1) {
                angularSpeed = -angularSpeed;
            }
        }

        if(randomInitialRotation) {
            int incrementCount = 360 / randomRotationIncrements;
            int randomIncrement = Mathf.FloorToInt(Random.value * incrementCount);
            RotateBy(randomIncrement * randomRotationIncrements);
        }
    }

    // Update is called once per frame
    void Update()
    {
        RotateBy(angularSpeed * Time.fixedDeltaTime);
    }

    void RotateBy(float angle) {
        transform.Rotate(Vector3.up * angle, Space.Self);
    }
}

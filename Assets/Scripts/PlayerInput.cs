﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Key
{
    none = -1, jump, dash, brake,
    forward, right, backward, left
}
public enum ControlScheme {hotMenuButtons,retroSpellwheel}


public class PlayerInput
{
    /*
     * NAMING CONVENTIONS
     * bool ___ - a button of that axis is currently pressed
     * bool ___Pressed - a button of that axis was just pressed
     * bool ___Released - a button of that axis was just released
     * bool ___Toggle - the state of a toggleable input, like a flashlight or similar
     * float ___Buffer - for when a button press is buffered. the game time in which the input should stop being buffered
     * float ___BufferMax - the time (seconds) that button should be buffered after it is pressed
     */
    public Vector3 move;
    public Vector3 mouse;

    int keycount;
    public Dictionary<Key, KeyState> keyDictionary;

    public PlayerInput(ControlScheme scheme = ControlScheme.hotMenuButtons)
    {
        keycount = System.Enum.GetNames(typeof(Key)).Length - 1; //-1 to account for "none" which is mapped to -1
        keyDictionary = new Dictionary<Key, KeyState>();

        keyDictionary[Key.jump] = new KeyState(KeyCode.Space);
        keyDictionary[Key.dash] = new KeyState(KeyCode.Mouse1);
        keyDictionary[Key.brake] = new KeyState(KeyCode.LeftShift);
        keyDictionary[Key.forward] = new KeyState(KeyCode.W);
        keyDictionary[Key.right] = new KeyState(KeyCode.D);
        keyDictionary[Key.backward] = new KeyState(KeyCode.S);
        keyDictionary[Key.left] = new KeyState(KeyCode.A);
    }
    public void Reset(bool hard = false)
    {
        //reset all default inputs
        mouse = Vector3.zero;

        if (hard)
        {
            move = Vector3.zero;
            for (int n = 0; n < keycount; n++)
            {
                Key nKey = (Key)n;
                keyDictionary[nKey].Reset();
            }
        }
        else
        {
            for (int n = 0; n < keycount; n++)
            {
                Key nKey = (Key)n;
                keyDictionary[nKey].pressed = false;
                keyDictionary[nKey].released = false;
            }
        }
    }
    public void Update()
    {
        mouse = new Vector3(-Input.GetAxisRaw("mousey"), Input.GetAxisRaw("mousex"), 0);
        
        for (int n = 0; n < keycount; n++)
        {
            keyDictionary[(Key)n].Check();
        }

        move = new Vector3(
            Extensions.BoolsToAxis(keyDictionary[Key.right].held, keyDictionary[Key.left].held), 
            0,
            Extensions.BoolsToAxis(keyDictionary[Key.forward].held, keyDictionary[Key.backward].held));
    }
    public bool GetKey(Key key) {
        return keyDictionary[key].held;
    }
    public bool GetKeyDown(Key key) {
        return keyDictionary[key].pressed;
    }
    public bool GetKeyUp(Key key) {
        return keyDictionary[key].released;
    }
}

public class KeyState
{
    public bool held;
    public bool pressed;
    public bool released;
    public KeyCode keyCode; 

    public KeyState(KeyCode _keyCode)
    {
        held = false;
        pressed = false;
        released = false;
        keyCode = _keyCode;
    }

    public void Reset()
    {
        held = false;
        pressed = false;
        released = false;
    }
    public void Check()
    {
        held = Input.GetKey(keyCode);
        pressed = Input.GetKeyDown(keyCode);
        released = Input.GetKeyDown(keyCode);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelController))]
public class LevelControllerInspector : Editor
{
    private LevelController self;

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        self = target as LevelController;

        if(GUILayout.Button("Generate Next Zone")) {
            self.AddNewZone();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BrushCannon))]
public class BrushCannonInspector : Editor
{
    BrushCannon self;
    float targetLaunchHeight;

    public override void OnInspectorGUI() {
        self = target as BrushCannon;
        

        Rect r = EditorGUILayout.BeginHorizontal();
        targetLaunchHeight = EditorGUILayout.FloatField("Target Height", targetLaunchHeight);
        if(GUILayout.Button("Recalculate")) {
            self.LaunchSpeed = self.CalculateLaunchSpeed(targetLaunchHeight);
        }
        EditorGUILayout.EndHorizontal();

        base.OnInspectorGUI();
    }
}

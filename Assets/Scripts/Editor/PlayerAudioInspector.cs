﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerAudio))]
public class PlayerAudioInspector : Editor
{
    public override void OnInspectorGUI() {
        PlayerAudio self = target as PlayerAudio;

        base.OnInspectorGUI();

        foreach (PlayerAudio.PlayerClips clip in System.Enum.GetValues(typeof(PlayerAudio.PlayerClips))) {
            self.SetClip(clip,EditorGUILayout.ObjectField(new GUIContent(clip.ToString()), self.GetClip(clip), typeof(AudioClip), false) as AudioClip);
        }
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Room))]
public class RoomInspector : Editor
{
    private Room room;

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        room = target as Room;

        if(room.EntranceTransform == null && room.ExitTransform == null) {
            if(GUILayout.Button("Create Entrance & Exit Transforms")) {

                //start undo event
                Undo.SetCurrentGroupName("Create Entrance & Exit Transforms");

                Transform tf = MakeTransformAsChild("Entrance");
                Undo.RegisterCreatedObjectUndo(tf.gameObject, "Create Entrance");
                room.EntranceTransform = tf;

                tf = MakeTransformAsChild("Exit");
                Undo.RegisterCreatedObjectUndo(tf.gameObject, "Create Exit");
                room.ExitTransform = tf;

                //end undo event
                Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

                EditorUtility.SetDirty(room);
            }
        }

        if (GUILayout.Button("Generate Next Room")) {
            Room r = room.MakeNextRoom();
            Selection.activeGameObject = r.gameObject;
        }
    }

    Transform MakeTransformAsChild(string name) {
        Transform tf = new GameObject(name).transform;
        tf.SetParent(room.transform);
        tf.localPosition = Vector3.zero;
        tf.localRotation = Quaternion.identity;
        tf.localScale = Vector3.one;
        return tf;
    }
}

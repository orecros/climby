﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    [HideInInspector] public CapsuleCollider CC;
    public Camera CAM;
    PlayerInput inputs;
    public Animator energyBarAnimator;
    Animator animator;

    [HideInInspector] public UnityEvent OnJump;
    [HideInInspector] public UnityEvent OnWallJump;
    [HideInInspector] public UnityEvent OnMissWallJump;
    [HideInInspector] public UnityEvent OnLand;
    [HideInInspector] public UnityEvent OnStartSliding;
    [HideInInspector] public UnityEvent OnStopSliding;

    // the vertical speed the player was moving last time they hit the ground
    [HideInInspector] public float LastLandingSpeed;

    private float WALL_LIMIT = 91;
    private float SLOPE_LIMIT = 45;
    public static float skinWidth = 0.05f;

    private float GRAVITY; //acceleration due to gravity
    private float JUMP_SPEED = 13f; //speed at which you leave the ground
    private float WALL_JUMP_SPEED = 15.5f; //wall jump magnitude
    float JUMP_BUFFER = 0.3f; //time in seconds preceding touching the ground or a wall where pressing the jump button will cause an instant jump
    float jumpBufferTime; //the time at which a buffered jump will expire
    
    //Modifiable movement variables
    private float AIR_SPEED_SOFTCAP = 20; //mps flattened speed to reach before applying friction
    private float WALK_SPEED = 6; //default movement speed
    private float WALK_ACCEL = 200; //how fast you get to max movespeed
    private float AIR_SPEED_MAX = 1.5f; //max tangent air speed, the lower this value the slower the airstrafe
    private float AIR_ACCELERATE = 150; //acceleration during air strafe
    
    //Dash Variables
    public float DASH_SPEED_DELTA;
    public float DASH_SPEED_MAX;
    public float ENERGY_REGEN_RATE;
    float energy;
    public float Energy {
        get => energy;
        set {
            energy = value;
            energyBarAnimator.SetFloat("chargeAmount", energy);
        }
    }

    public enum State { GROUNDED, AIRBORNE, DEAD };
    private State playerState = State.GROUNDED;
    public State PlayerState {
        get { return playerState; }
        set {
            State oldState = playerState;
            playerState = value;

            if (playerState == State.GROUNDED) {
                if (oldState == State.AIRBORNE) {
                    LastLandingSpeed = vel.y * -1;
                    StopSliding();
                    OnLand.Invoke();
                }
            }
            else if (playerState == State.DEAD) {
                StopSliding();
                SeverSurfaceConnections();
            }
            else { // if(value == State.AIRBORNE) {
                if(oldState == State.GROUNDED) {
                    FloorSurface = null;
                }
            }
            return;
        }
    }

    private bool isSliding = false; //is there a wall to wall jump off of?
    Surface slidingWall; //the surface of the wall
    [HideInInspector] public Vector3 vel;
    [HideInInspector] public Vector3 referenceVelocity; // the speed the player's reference frame is moving in
    Surface floorSurface;
    public Surface FloorSurface {
        get { return floorSurface; }
        set {
            if (floorSurface != null) {
                if (floorSurface.Brush != null) {
                    floorSurface.Brush.ExitContact(this, Brush.ContactType.Floor);
                }
            }

            floorSurface = value;
            if(floorSurface != null && floorSurface.Brush != null) {
                floorSurface.Brush.EnterContact(this, Brush.ContactType.Floor);
            }
        }
    }
    Surface wallSurface;
    public Surface WallSurface {
        get { return wallSurface; }
        set {
            if (wallSurface != null) {
                if (wallSurface.Brush != null) {
                    wallSurface.Brush.ExitContact(this, Brush.ContactType.Wall);
                }
            }

            wallSurface = value;
            if (wallSurface != null && wallSurface.Brush != null) {
                wallSurface.Brush.EnterContact(this, Brush.ContactType.Wall);
            }
        }
    }
    
    public enum GroundMoveType {
        Walking,
        Skating
    };
    GroundMoveType _groundMoveType = GroundMoveType.Walking;
    public GroundMoveType groundMoveType {
        get { return _groundMoveType; }
        set {
            if (_groundMoveType == value) return;
            _groundMoveType = value;
            animator.SetBool("IsSkating", value == GroundMoveType.Skating);
        }
    }

    //aiming
    private float SENSITIVITY = 0.5f; //default: 0.5. mouselook speed
    private float SENS_PITCH_SCALE = 0.8f; //fraction of sensitivity to apply for pitch
    public Vector3 eyeAngles; //player's aim direction, in euler angles

    private void Start() {
        GRAVITY = -(Physics.gravity.y);
        CC = GetComponent<CapsuleCollider>();
        animator = GetComponent<Animator>();
        Energy = 1;
        jumpBufferTime = 0;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        eyeAngles = transform.eulerAngles;
        transform.rotation = Quaternion.identity;
        inputs = new PlayerInput();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Tab)) {
            Debug.Break();
        }
        inputs.Update();
        DoLook();

        //buffer jump
        if (inputs.GetKeyDown(Key.jump)) {
            jumpBufferTime = Time.time + JUMP_BUFFER;
        }

        //check player's state and do whatever based on their state
        if (PlayerState == State.GROUNDED) {
            DoEnergyRegen();
            if(inputs.GetKeyDown(Key.dash) && Energy >= 1) {
                DoDash();
            }

            //if the player happens to have jump buffered while grounded, just jump right away
            if (jumpBufferTime > Time.time) {
                jumpBufferTime = 0;
                DoJump();
            }
            else {
                // if the player has the brake key held when they land, break right away
                if (groundMoveType == GroundMoveType.Skating && inputs.GetKey(Key.brake)) DoBrake();

                //putting this here means if you have jump buffered, you won't lose speed from the one ground frame
                DoGroundMovement();
            }
            DoAux();
            UpdateWallSurface();
        }
        else if (PlayerState == State.AIRBORNE) {
            DoAirMovement();
            DoAux();
            UpdateWallSurface();
            DoSlide();
        }
        else if (PlayerState == State.DEAD) {
            Move(vel * Time.deltaTime);
        }
    }
    
    public Quaternion GetAimQuat() {
        return Quaternion.Euler(eyeAngles);
    }
    /// <summary>
    /// Return a rotation of the player's aim direction, flattened
    /// </summary>
    /// <returns></returns>
    public Quaternion GetYawQuat() {
        return Quaternion.Euler(0, eyeAngles.y, 0);
    }
    /// <summary>
    /// Returns a vector along player's aim direction
    /// </summary>
    /// <returns name = aimDir></returns>
    public Vector3 GetAimDir() {
        return GetAimQuat() * Vector3.forward;
    }
    /// <summary>
    /// returns the camera's position
    /// </summary>
    /// <returns></returns>
    public Vector3 GetShootPos() {
        return transform.position + CC.radius * Vector3.up;
    }
    public Vector3 GetAdjustedVelocity() {
        return vel - referenceVelocity;
    }
    
    public void Kill(DeathType deathType) {
        Transform checkpoint = LevelController.Instance.GetCurrentCheckpoint();

        transform.position = checkpoint.position + Vector3.up * 1.07f;
        eyeAngles = checkpoint.eulerAngles;
        vel = Vector3.zero;

        jumpBufferTime = 0;

        PlayerState = State.DEAD;
        PlayerState = State.AIRBORNE;
        groundMoveType = GroundMoveType.Walking;
    }

    // math functions
    public bool EyeCast(Vector3 direction, float maxDistance, out RaycastHit hitInfo, int layerMask = (int)LAYERMASK.SOLIDS_ONLY) {
        if (Physics.Raycast(CAM.transform.position, direction, out hitInfo, maxDistance, layerMask, UnityEngine.QueryTriggerInteraction.Ignore)) {
            return true;
        }
        return false;
    }
    public bool EyeCastSphere(Vector3 direction, float maxDistance, out RaycastHit hitInfo, int layerMask = (int)LAYERMASK.SOLIDS_ONLY) {
        return SimpleCastSphere(GetShootPos(), direction, maxDistance, out hitInfo, layerMask);
    }
    public bool SimpleCastSphere(Vector3 position, Vector3 direction, float maxDistance, out RaycastHit hitInfo, int layerMask = (int)LAYERMASK.SOLIDS_ONLY) {
        if (Physics.SphereCast(position, CC.radius, direction, out hitInfo, maxDistance, layerMask, UnityEngine.QueryTriggerInteraction.Ignore)) {
            return true;
        }
        return false;
    }
    /// <summary>
    /// performs a capsuleCast fitting the character's shape
    /// </summary>
    /// <param name="position">initial position (transform.position)</param>
    /// <param name="direction">unit direction to travel in</param>
    /// <param name="distance">float distance to travel for</param>
    /// <param name="hitInfo">RaycastHit info to return</param>
    /// <param name="backstep">how far back from initial point to start</param>
    /// <returns name = didHit>Whether or not a surface was hit</returns>
    public bool SimpleCast(Vector3 position, Vector3 direction, float distance, out RaycastHit hitInfo, int layermask = (int)LAYERMASK.SOLIDS_ONLY) {
        Vector3 p1 = position + CC.center + Vector3.down * (CC.height * .5f - CC.radius);
        Vector3 p2 = p1 + Vector3.up * (CC.height - CC.radius * 2);
        bool didHit = Physics.CapsuleCast(p1, p2, CC.radius, direction, out hitInfo, distance, layermask, UnityEngine.QueryTriggerInteraction.Ignore);

        return didHit;
    }
    public bool SimpleCast(Ray ray, float distance, out RaycastHit hitInfo, int layermask = (int)LAYERMASK.SOLIDS_ONLY) {
        Vector3 pos = ray.origin;
        Vector3 dir = ray.direction;
        return SimpleCast(pos, dir, distance, out hitInfo, layermask);
    }
    public bool SimpleCheck(Vector3 position, int layerMask = (int)LAYERMASK.SOLIDS_ONLY) {
        Vector3 p1 = position + CC.center + Vector3.down * (CC.height * .5f - CC.radius);
        Vector3 p2 = p1 + Vector3.up * (CC.height - CC.radius * 2);
        return Physics.CheckCapsule(p1, p2, CC.radius, layerMask, UnityEngine.QueryTriggerInteraction.Ignore);
    }
    public Collider[] SimpleOverlap(Vector3 position, int layermask = (int)LAYERMASK.SOLIDS_ONLY) {
        Vector3 p1 = position + CC.center + Vector3.down * (CC.height * .5f - CC.radius);
        Vector3 p2 = p1 + Vector3.up * (CC.height - CC.radius * 2);
        return Physics.OverlapCapsule(p1, p2, CC.radius, layermask, UnityEngine.QueryTriggerInteraction.Ignore);
    }

    // physics functions
    public void Shove(Vector3 movement, float interval) {
        //move the player the correct distance.
        transform.position += movement;
        ApplyForce(movement.normalized, movement.magnitude / interval);
    }
    /// <summary>
    /// Shoves the player to the specified point, making sure they are moving at least as fast, if it traveled movement distance every interval
    /// </summary>
    /// <param name="newPos">the point to move to</param>
    /// <param name="interval">over what duration the force was applied (doesnt actually happen over a duration, it's Zto calculate change in velocity)</param>
    public void ShoveTo(Vector3 newPos, float interval) {
        //but it was me, DIO-- i mean Shove() in disguise
        Vector3 movement = newPos - transform.position;
        Shove(movement, interval);
    }
    public void ApplyForce(Vector3 direction, float maxSpeed) {
        float curSpeed = Vector3.Dot(vel, direction);
        //weird step here from the quake movement code guys (guy?)
        //really, i want to see if movement.magnitude * interval is bigger than curSpeed
        //that would mean the player isn't already moving fast enough in that direction
        //then, if it is, add the amount it is bigger by the player's speed along movement.normalized
        //this just cuts out a step because lol
        //also fun fact I stumbled upon writing the exact same code line for line doing a completely different task
        //until this step when I realized what I was doing
        float addSpeed = maxSpeed - curSpeed;
        if (addSpeed <= 0) {
            //already going too fast, so who cares
            return;
        }

        //since I'm not going too fast, make me go just the right speed in that direction
        vel += addSpeed * direction;

        //make the player airborne just in case they are going to leave the ground
        //if they aren't, they will just be made grounded again immediately
        PlayerState = State.AIRBORNE;
    }

    // internal functions
    void Move(Vector3 movement) {
        if (movement.magnitude < 0.003) {
            //if you aren't moving, at least check if there's ground below you still
            CheckForGround();
            return;
        }

        bool groundFound = false;

        Vector3 mov = movement;
        while (mov.magnitude >= 0.003) {
            RaycastHit hit;
            bool didHit = SimpleCast(transform.position, mov.normalized, mov.magnitude + skinWidth, out hit);

            if (didHit) {
                float angle = Vector3.Angle(Vector3.up, hit.normal);
                bool isFloor = (angle < SLOPE_LIMIT);
                bool isWall = !isFloor && angle < WALL_LIMIT;

                // check if we hit a new floor or wall
                if (isFloor) {
                    CheckNewFloor(transform.position, hit.normal, hit.collider);
                }
                else if (isWall) {
                    CheckNewWall(transform.position, hit.normal, hit.collider);
                }

                //move up to the object
                transform.position += (hit.distance - skinWidth) * mov.normalized;

                //move back from the collision point

                //remove distance moved from mov
                mov -= (hit.distance - skinWidth) * mov.normalized;

                //remove non-tangent component from mov AND vel
                mov = mov.GetTangentComponent(hit.normal);
                vel = vel.GetTangentComponent(hit.normal);

                //if you're airborne, try and slide on or stand on the surface
                if (PlayerState == State.AIRBORNE) {

                    //if you aren't already sliding, see if you can slide on this surface
                    if (!isSliding) {

                        if (isWall) {
                            StartSliding(new Surface(transform.position, hit.normal));
                        }
                    }

                    //check if this is a surface you can stand on
                    if (isFloor) {
                        PlayerState = State.GROUNDED;
                        groundFound = true;
                    }
                }
                else if (PlayerState == State.GROUNDED) {

                    if (isFloor) {
                        groundFound = true;
                    }
                }

            }
            else {
                transform.position += mov;
                mov = Vector3.zero;
            }
        }

        if (PlayerState == State.GROUNDED && !groundFound) {
            CheckForGround();
        }
        return;
    }
    void DoGroundMovement() {
        

        if(groundMoveType == GroundMoveType.Walking) {
            //Change vel based on inputs
            Vector3 movementTransformed = (GetYawQuat() * inputs.move).normalized;
            vel = GroundAccelerate(vel - referenceVelocity, movementTransformed) + referenceVelocity;
        }
        else if(groundMoveType == GroundMoveType.Skating) {
            if (inputs.GetKeyDown(Key.brake)) {
                DoBrake();
            }

            //Change vel based on inputs
            Vector3 airMove = GetYawQuat() * (new Vector3(inputs.move.x, 0, inputs.move.z));
            vel = AirAccelerate(vel, airMove);
            
            //apply gravity
            vel += Vector3.down * GRAVITY * Time.deltaTime;
        }


        //check the slope of the ground
        RaycastHit hitInfo;
        bool didHit = SimpleCast(transform.position, Vector3.down, 0.01f + skinWidth, out hitInfo);
        if (didHit) {
            transform.position += (hitInfo.distance - skinWidth) * Vector3.down;
            if (Vector3.Angle(hitInfo.normal, Vector3.up) <= SLOPE_LIMIT) {
                vel = vel.GetTangentComponent(hitInfo.normal);
            }
        }

        //move
        Move(vel * Time.deltaTime);        
    }
    void DoAirMovement() {
        //Change vel based on inputs
        Vector3 airMove = GetYawQuat() * (new Vector3(inputs.move.x, 0, inputs.move.z));
        vel = AirAccelerate(vel, airMove);

        //apply gravity
        vel += Vector3.down * GRAVITY * Time.deltaTime;

        //apply soft speedcap
        float overSpd = vel.Flatten().magnitude - AIR_SPEED_SOFTCAP;
        if (overSpd > 0) {
            //don't SNAP speed to the speed limit, but make it shift in that direction
            vel -= vel.Flatten().normalized * overSpd * Time.deltaTime;
        }

        //move
        Move(vel * Time.deltaTime);
    }
    void DoLook() {
        //change eyeAngles based on mouse
        eyeAngles += inputs.mouse.Scale(SENSITIVITY * SENS_PITCH_SCALE, SENSITIVITY, SENSITIVITY);
        //clamp pitch
        eyeAngles.x = Mathf.Clamp(eyeAngles.x, -89.9f, 89.9f);

        CAM.transform.rotation = GetAimQuat();

        //orient player model towards camera
        Vector3 dir = GetAimDir().Flatten();
        if (dir == Vector3.zero) {
            dir = GetAimQuat() * Vector3.down;
        }
    }
    void DoAux() {
        
    }
    
    // Surface functions
    /// <summary>
    /// compare this new collider with our cached wall and update as needed
    /// </summary>
    /// <param name="hitPosition"></param>
    /// <param name="hitNormal"></param>
    /// <param name="collider"></param>
    public void CheckNewWall(Vector3 hitPosition, Vector3 hitNormal, Collider collider) {
        // first, we should check if this wall is the same as our currently cached surface
        if(CompareColliderToSurface(collider, WallSurface)) {
            // just update the collision point for our wall and return
            WallSurface.UpdateCollisionPoint(hitPosition, hitNormal);
            return;
        }

        // if our collider is different, we should update our collider
        WallSurface = new Surface(hitPosition, hitNormal, collider);
    }
    /// <summary>
    /// compare this new collider with our cached floor and update as needed
    /// </summary>
    /// <param name="hitPosition"></param>
    /// <param name="hitNormal"></param>
    /// <param name="collider"></param>
    public void CheckNewFloor (Vector3 hitPosition, Vector3 hitNormal, Collider collider) {
        // first, we should check if this wall is the same as our currently cached surface
        if (CompareColliderToSurface(collider, FloorSurface)) {
            // just update the collision point for our wall and return
            FloorSurface.UpdateCollisionPoint(hitPosition, hitNormal);
            return;
        }

        // if our collider is different, we should update our collider
        FloorSurface = new Surface(hitPosition, hitNormal, collider);
    }
    public bool CompareColliderToSurface(Collider collider, Surface surface) {
        if (surface == null) return false;

        return (surface.Collider == collider);
    }
    private void UpdateWallSurface() {
        if (WallSurface == null) return;

        //if you've gotten too far from the wall
        if (Vector3.Dot(transform.position - WallSurface.Position, WallSurface.Normal) > 0.1) {
            //remove the surface
            WallSurface = null;
            return;
        }
    }
    private void SeverSurfaceConnections() {
        if (WallSurface != null && WallSurface.Brush != null) {
            WallSurface.Brush.SeverContact(this, Brush.ContactType.Wall);
        }
        if (FloorSurface != null && FloorSurface.Brush != null) {
            FloorSurface.Brush.SeverContact(this, Brush.ContactType.Floor);
        }
    }

    // Sliding functions
    void DoSlide() {
        if (!isSliding) return;

        //if you've gotten too far from the wall, stop sliding
        if (Vector3.Dot(transform.position - slidingWall.Position, slidingWall.Normal) > 0.1) {
            StopSliding();
            return;
        }

        //check if the wall is still there
        RaycastHit hit;
        if (!SimpleCast(transform.position, -slidingWall.Normal, 0.5f, out hit)) {
            /*
             * the 0.5f cast distance might seem a little extreme, but because of the statement above this one
             * the only situation where this could cause a problem is with 2 parallel walls where the second one
             * is slightly further back, which would be pretty rare, if at all existent architecture
             */
            //if NOTHING AT ALL is there, stop sliding
            StopSliding();
            return;
        }
        else {
            //if there is SOMETHING there, see if you can slide on it
            float angle = Vector3.Angle(Vector3.up, hit.normal);
            if (angle <= 90.1f && angle > SLOPE_LIMIT) {
                //if you can, then replace the listed sliding wall
                slidingWall = new Surface(transform.position, hit.normal);
            }
            else {
                //if you can't, then stop sliding
                StopSliding();
                return;
            }
        }

        //check if the player tried to walljump

        if (jumpBufferTime > Time.time) {
            jumpBufferTime = 0;
            if (Energy >= 1)
                DoWallJump(slidingWall.Normal);
            else {
                // missed walljump
                OnMissWallJump.Invoke();
            }
        }

    }
    void StopSliding() {
        if (!isSliding)
            return;
        isSliding = false;
        OnStopSliding.Invoke();
    }
    void StartSliding(Surface newSurface) {
        if (isSliding) return;
        isSliding = true;
        slidingWall = newSurface;
        OnStartSliding.Invoke();
    }
    void DoWallJump(Vector3 wallNormal) {
        //become airborne if you aren't already
        PlayerState = State.AIRBORNE;

        //add velocity
        vel += slidingWall.Normal / 2 * WALL_JUMP_SPEED;
        vel.y = WALL_JUMP_SPEED;

        OnWallJump.Invoke();

        Energy = 0;
    }


    Vector3 GroundAccelerate(Vector3 velocity, Vector3 mov) {
        float curTopSpeed = WALK_SPEED; //if holding +speed, use WALK_SPEED else use RUN_SPEED
        return Vector3.MoveTowards(velocity, mov * curTopSpeed, WALK_ACCEL * Time.deltaTime);

    }
    /// <summary>
    /// Airstrafing function from quake 3 movement code
    /// </summary>
    /// <param name="vel">Initial Velocity</param>
    /// <param name="mov">player's movement input</param>
    /// <returns>New velocity</returns>
    Vector3 AirAccelerate(Vector3 vel, Vector3 mov) {
        return AddVelocityDirectional(vel, mov, AIR_ACCELERATE * Time.deltaTime, AIR_SPEED_MAX);
    }
    /// <summary>
    /// Increase our velocity in target direction by maxDelta up to maxAbsolute
    /// </summary>
    /// <param name="currentVel"></param>
    /// <param name="direction"></param>
    /// <param name="addSpeed"></param>
    /// <param name="maxAbsolute"></param>
    /// <returns></returns>
    Vector3 AddVelocityDirectional(Vector3 currentVel, Vector3 direction, float addDelta, float maxAbsolute) {
        Vector3 flatVel = currentVel.Flatten();

        // get how fast we are currently going in our target direction
        float currentspeed = Vector3.Dot(flatVel, direction);

        float maxAddDelta = maxAbsolute - currentspeed;

        // if we are already going too fast, do not modify our speed at all
        if (maxAddDelta <= 0) return vel;

        // clamp our addDelta
        addDelta = Mathf.Min(addDelta,maxAddDelta);

        return vel + addDelta * direction;

    }

    void DoJump() {
        vel.y = JUMP_SPEED;
        PlayerState = State.AIRBORNE;

        OnJump.Invoke();
    }
    void DoDash() {
        groundMoveType = GroundMoveType.Skating;

        Vector3 movementTransformed = (GetYawQuat() * inputs.move).normalized;
        vel = AddVelocityDirectional(vel, movementTransformed, DASH_SPEED_DELTA, DASH_SPEED_MAX);

        Energy = 0;
    }
    void DoBrake() {
        groundMoveType = GroundMoveType.Walking;
    }
    void CheckForGround() {
        //if you didn't hit the ground with the move, double check to see if there is still ground below you
        //note CollisionFlags.Below just means you hit SOMETHING below you, up to 89.999 degree slope. code accordingly
        RaycastHit hit;
        if (SimpleCast(transform.position, Vector3.down, 0.01f + skinWidth, out hit)) {
            float angle = Vector3.Angle(Vector3.up, hit.normal);
            bool isFloor = (angle < SLOPE_LIMIT);
            bool isWall = !isFloor && angle < WALL_LIMIT;

            // check if we hit a new floor or wall
            if (isFloor) {
                CheckNewFloor(transform.position, hit.normal, hit.collider);
            }
            else if (isWall) {
                CheckNewWall(transform.position, hit.normal, hit.collider);
            }

            if (!isFloor) {
                PlayerState = State.AIRBORNE;
            }
            else {
                transform.position += (hit.distance - skinWidth) * Vector3.down;
            }
        }
        else {
            PlayerState = State.AIRBORNE;
        }
    }

    void DoEnergyRegen() {
        Energy = Mathf.Min(1,energy + ENERGY_REGEN_RATE * Time.deltaTime);
    }
}

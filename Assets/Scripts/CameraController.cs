﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera target;
    public float speedFieldShift;
    public float speedFieldUpshiftDuration;
    public float speedFieldDownshiftDuration;

    Coroutine speedFieldShiftRoutine;
    enum FieldShiftState {
        normal,
        shifted,
        shifting,
        unshifting
    }
    FieldShiftState shiftState = FieldShiftState.normal;

    float defaultFOV;
    

    private void Awake() {
        defaultFOV = target.fieldOfView;
    }

    public void SetFovShifted(int shifted) {

        if(shifted != 0) {
            if(shiftState == FieldShiftState.normal) {
                speedFieldShiftRoutine = StartCoroutine(shiftFOV(defaultFOV, defaultFOV + speedFieldShift, speedFieldUpshiftDuration));
                shiftState = FieldShiftState.shifting;
            }
            else if(shiftState == FieldShiftState.unshifting) {
                StopCoroutine(speedFieldShiftRoutine);
                speedFieldShiftRoutine = StartCoroutine(shiftFOV(target.fieldOfView, defaultFOV + speedFieldShift, speedFieldUpshiftDuration));
                shiftState = FieldShiftState.shifting;
            }
        }
        else {
            if (shiftState == FieldShiftState.shifted) {
                speedFieldShiftRoutine = StartCoroutine(shiftFOV(target.fieldOfView, defaultFOV, speedFieldUpshiftDuration));
                shiftState = FieldShiftState.unshifting;
            }
            else if (shiftState == FieldShiftState.shifting) {
                StopCoroutine(speedFieldShiftRoutine);
                speedFieldShiftRoutine = StartCoroutine(shiftFOV(target.fieldOfView, defaultFOV, speedFieldUpshiftDuration));
                shiftState = FieldShiftState.unshifting;
            }
        }
    }

    IEnumerator shiftFOV(float initialFOV, float finalFOV, float duration) {
        float startTime = Time.time;

        while(Time.time < startTime+duration) {
            target.fieldOfView = Mathf.SmoothStep(initialFOV, finalFOV, (Time.time - startTime) / duration);

            //wait another frame
            yield return new WaitForEndOfFrame();
        }
        target.fieldOfView = finalFOV;


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LAYERMASK
{
    SOLIDS_ONLY = (1 << 0),
    SOLIDS_AND_PROJECTILES = ~((1 << 8) + (1 << 9)),
    PLAYERS_ONLY = (1 << 11),
    PLAYERS_AND_PROJECTILES = (1 << 10) + (1 << 11),
    SPECIAL_ONLY = (1 << 3)
};
public enum LAYER
{
    Default = 0,
    TransparentFX = 1,
    IgnoreRaycast = 2,
    Special = 3,
    Water = 4,
    UI = 5,
    Special2 = 6,
    Special3 = 7,
    noselfcam = 8,
    trigger = 9,
    projectile = 10,
    player = 11
}
static class Extensions
{
    public static float BoolsToAxis(bool pos, bool neg) {
        //else return 1 or -1 (or 0) depending on if pos or neg was pressed
        return (pos == neg) ? 0 : (pos) ? 1 : -1;
    }

    /// <summary>
    /// plugs X into my sharpCurve function, slowly fades before suddenly spiking
    /// </summary>
    /// <param name="x">a value between 0 & 1</param>
    /// <returns>a value between 0 & 1</returns>
    public static float SharpCurve(float x)
    {
        if(x < .75f)
        {
            return 1 - 4 * x / 3;
        }
        else
        {
            return 4 * x - 3;
        }
    }

    /// <summary>
    /// remove the Y component from the vector3
    /// </summary>
    /// <param name="vec"></param>
    /// <returns></returns>
    public static Vector3 Flatten(this Vector3 vec)
    {
        vec.y = 0;
        return vec;
    }

    /// <summary>
    /// Scales each component of a Vector3 by another vector3's components
    /// </summary>
    /// <param name="vec"></param>
    /// <param name="other"></param>
    /// <returns></returns>
    public static Vector3 Scale(this Vector3 vec, Vector3 other)
    {
        vec.x *= other.x;
        vec.y *= other.y;
        vec.z *= other.z;
        return vec;
    }

    /// <summary>
    /// Scale each component of a vector3 by respective x/y/z components
    /// </summary>
    /// <param name="vec"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public static Vector3 Scale(this Vector3 vec, float x, float y, float z)
    {
        vec.x *= x;
        vec.y *= y;
        vec.z *= z;
        return vec;

    }
    /// <summary>
    /// gets the component of velocity along the slope of the surface
    /// </summary>
    /// <param name="vec"></param>
    /// <param name="slopeNormal"></param>
    /// <returns>the new velocity to use</returns>
    public static Vector3 GetTangentComponent(this Vector3 vec, Vector3 slopeNormal)
    {
        return vec - vec.GetNormalComponent(slopeNormal);
    }
    /// <summary>
    /// gets the component of velocity pointing away from the surface
    /// </summary>
    /// <param name="vec"></param>
    /// <param name="normal"></param>
    /// <returns></returns>
    public static Vector3 GetNormalComponent(this Vector3 vec, Vector3 normal)
    {
        return Vector3.Dot(vec, normal) * normal;
    }

    /// <summary>
    /// Clamps the magnitude of the vector to between the min and max size
    /// </summary>
    /// <param name="vec">a nonzero vector</param>
    /// <param name="min">minimum magnitude</param>
    /// <param name="max">maximum magnitude</param>
    /// <returns></returns>
    public static Vector3 Clamp(this Vector3 vec, float min, float max)
    {
        if(vec == Vector3.zero)
        {
            return Vector3.zero;
        }
        float magnitude = Mathf.Clamp(vec.magnitude,min,max);

        return vec.normalized * magnitude;

    }

}

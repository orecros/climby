﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerAudio : MonoBehaviour
{
    AudioSource source;

    public enum PlayerClips {
        StartSkatingSound = 0,
        StartWalkingSound
    }
    [HideInInspector] public AudioClip[] AudioClips;

    private void Awake() {
        source = GetComponent<AudioSource>();
    }

    public AudioClip GetClip(PlayerClips clip) {
        return AudioClips[(int)clip]?? null;
    }
    public void SetClip(PlayerClips clip, AudioClip audioClip) {
        AudioClips[(int)clip] = audioClip;
    }

    public void PlayClip(PlayerClips clip) {
        source.PlayOneShot(GetClip(clip),1);
    }

    private void OnValidate() {
        int length = Enum.GetNames(typeof(PlayerClips)).Length;
        if (AudioClips.Length != length) {
            AudioClip[] a = new AudioClip[length];

            Array.Resize(ref AudioClips, length);
        }
    }
}
